CREATE DATABASE IF NOT EXISTS pokemonai;

USE pokemonai;

CREATE TABLE IF NOT EXISTS raw (
    id varchar(20),
    raw text,
    PRIMARY KEY (id)
);
