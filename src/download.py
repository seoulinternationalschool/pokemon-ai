import json
import urllib.parse
import urllib.request
import ssl
import traceback
import re
import csv
import MySQLdb
from time import strftime, localtime, time

start_timer = time()
start_time = strftime('%Y-%m-%d %H:%M:%S', localtime())
BATTLE_FORMAT = 'gen7ou'

# Logging stuff, open the file in appending mode,
# this will log content + time
log_txt = open('pokemonai.log', 'a+')

def log(s):
    now = strftime('%Y-%m-%d %H:%M:%S', localtime())
    log_txt.write(now + '  ' + s)

# Set up our database connections
try:
    db_connection = MySQLdb.connect(host='localhost',
                                    database='pokemonai',
                                    user='root')
except: 
    log('!! Could not establish a connection to the database via python.\n')
    exit()

cursor = db_connection.cursor()

# Now grabbing data
replay_urls = []
output_data = []
url_pattern = re.compile(r'/gen7ou-[0-9]+')

# Get list of replay urls like /gen7ou-3931235 or something
# from the replay.pokemonshowdown.com search page. THe 
# pagination limit is 10 pages.
for x in range(10):
    url = 'https://replay.pokemonshowdown.com/search?format=' + BATTLE_FORMAT + '&output=html&page=' + str(x);
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    response = urllib.request.urlopen(req).read().decode('utf-8')
    replay_urls.extend(url_pattern.findall(response))

# Get the log data from all of the links gathered above
ind = 1
total_number = len(replay_urls)

for x in replay_urls:
    ind2 = int(ind / total_number * 20)   
    url = 'https://replay.pokemonshowdown.com' + x + '.log'
    print('Getting URL {} of {}: '.format(ind, total_number) + url + ' {:.1f}% [{}{}]'.format(ind/total_number*100, 'x'*ind2, ' '*(20-ind2)), end='\r') # loading bar length = 20
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    response = urllib.request.urlopen(req).read().decode('utf-8')
    output_data.append((x, response))
    ind += 1

# For now, insert the data into the database using the url id as primary key
sql_prompt = "INSERT IGNORE INTO raw (id, raw) VALUES ('{}', N'{}')"

for d_tuple in output_data:
    try:
        tag = d_tuple[0]
        info = d_tuple[1].replace("'", "''") #must escape apostrophes
        cursor.execute(sql_prompt.format(tag, info))
        db_connection.commit()
    except:
        db_connection.rollback()
        print(d_tuple[0], d_tuple[1].encode('utf-8'))
        log('!! Was unable to add a listing to the database for log file {}, executed connection-rollback. TRACEBACK: \n{}\n'.format(d_tuple[0]))
        log('!! TRACEBACK: {}\n'.format(traceback.format_exc()))
        exit()
elapse = (time() - start_timer) / 1000
data_len = len(output_data)
end_time = strftime('%Y-%m-%d %H:%M:%S', localtime())
log('OK Log information was added to database. {} entries were added. Operation started at {}, ending at {}, taking {}ms'.format(data_len, 
                                                                                                                                 start_time,
                                                                                                                                 end_time,
                                                                                                                                 elapse))

