# Intended to parse pokemon showdown logs taken from the database
# WIP 
import MySQLdb

sql = MySQLdb.connect(name='pokemonai', user='root') 
sql_c = sql.cursor() 


class ShowdownGame: 
    def __init__(self, log):
        self.joined = []
        self.players = {'p1': {'name': '', 'rating': 0}, 'p2': {'name': '', 'rating': 0}}
        self.parse(log)
        
    def parse(log_string):
        actions = []
        log_chunks = log_string.split('\n')
        
        for chunk in log_chunks:
            info = chunk.split('|')
            op = info[1]
            param = info[1:]

            actions.append((op, param))

        for action in actions:
            # indicates p1/p2
            if action[0] == 'player':
                self.players[action[1][0]]['name'] = action[1][1]
                self.players[action[1][0]]['rating'] = action[1][2]
            
            # indicates a joining action, including p1 and p2
            else if action[0] == 'j': 
                self.joined.append(action[1][0])
            
            # indicates the teamsize of each player
            else if action[0] == 'teamsize':
                
